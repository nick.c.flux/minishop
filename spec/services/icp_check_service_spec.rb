require 'rails_helper'

RSpec.describe IcpCheckService do
  subject {described_class.new("123456789012345")}

  #valid state
  context 'icp is valid' do
    #let(:icp) { '123456789012345'}
    it 'is a valid icp' do
      subject.icp = "123456789012345"
      expect(subject.call).to be(false)
    end
  end

  context 'icp is invalid' do
    it 'has a letter as first character' do
      subject.icp = "Z12345678901234"
      expect(subject.call).to be(true)
    end

    it 'has a zero as last character' do
      subject.icp = "123456789012340"
      expect(subject.call).to be(true)
    end

    it 'has three repeating characters' do
      subject.icp = "111234567890123"
      expect(subject.call).to be(true)
    end
  end



end
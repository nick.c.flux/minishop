require 'rails_helper'

RSpec.describe SessionsController do
  fixtures :users

  def create_user_session
    post :create, params: {session: { username: username }}
  end

  def delete_user_session
    delete :delete
  end

  let(:username) { 'admin' }
  let(:user) { users(:one)}

  describe "#create" do
    context 'creating user session' do
      context 'user does not exist' do
        let(:username) {'fakeacount'}

        it 'is a username that does not exist' do
          create_user_session

          expect(session[:user_id]).to be_nil
          expect(response).to have_http_status(:ok)
        end
      end

      context 'user exists' do
        it 'is a valid username and session is made' do
          create_user_session

          new_user = User.new
          allow(new_user).to receive(:id).and_return(user)
          expect(new_user.id).to eq user

          expect(session[:user_id]).to eq 1
          expect(response).to have_http_status(:found)
          expect(response).to redirect_to :action => 'show', :controller => 'users', :id => user.id
        end
      end
    end
  end

  describe "#delete" do
    context 'destroying user session' do
      before do
        create_user_session
      end

      it 'destroys user session and redirects to login page' do
        delete_user_session

        expect(session[:user_id]).to be_nil
        expect(response).to redirect_to login_path
      end
    end
  end
end

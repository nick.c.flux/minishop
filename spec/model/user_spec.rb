require 'rails_helper'

RSpec.describe User do
  subject { described_class.new(
    username: "testusername",
    name: "test name"
  )}

  #valid case
  it 'is valid with valid attributes' do
    expect(subject).to be_valid
  end

  #username not entered
  it 'is not valid without username' do
    subject.username = nil
    expect(subject).to_not be_valid
  end

end
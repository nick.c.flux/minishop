require 'rails_helper'

RSpec.describe Request do
  subject { described_class.new(
    customer_name: "test name",
    email: "testemail@test.com",
    phone: "0212115432",
    customer_address1: "123 fake street",
    customer_postcode: "4321",
    customer_city: "Wellington",
    customer_icp: "54321ACBDE67890"

    )}

  #valid case
  it 'is valid with valid attributes' do
    expect(subject).to be_valid
  end
  #name validation
  it 'is not valid without a name' do
    subject.customer_name = nil
    expect(subject).to_not be_valid
  end

  #email validation
  it 'is not valid without an email' do
    subject.email = nil
    expect(subject).to_not be_valid
  end
  it 'is not valid without @ in email' do
    subject.email = "emailwithoutatdotcom"
    expect(subject).to_not be_valid
  end

  #phone validation
  it 'is not valid without a phone number' do
    subject.phone = nil
    expect(subject).to_not be_valid
  end
  it 'is not valid with text in phone number' do
    subject.phone = "phonenumber"
    expect(subject).to_not be_valid
  end

  #address1 validation
  it 'is not valid without an address' do
    subject.customer_address1 = nil
    expect(subject).to_not be_valid
  end

  #postcode validation
  it 'is not valid without a postcode' do
    subject.customer_postcode = nil
    expect(subject).to_not be_valid
  end
  it 'is not valid with a long postcode' do
    subject.customer_postcode = "12345"
    expect(subject).to_not be_valid
  end

  #city validation
  it 'is not valid without a city' do
    subject.customer_city = nil
    expect(subject).to_not be_valid
  end

  #icp validation
  it 'is not valid without an icp' do
    subject.customer_icp = nil
    expect(subject).to_not be_valid
  end
  it 'is not valid without a well formatted icp' do
    subject.customer_icp = "shortICP123"
    expect(subject).to_not be_valid
  end

end
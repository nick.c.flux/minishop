# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).

User.create!("username"=>"admin", "name"=>"admin name", "admin"=>true)
User.create!("username"=>"notadmin", "name"=>"not admin", "admin"=>false)

IcpRecord.create("id"=>1, "address1"=>"43 Hanson st", "suburb"=>"Newtown", "city"=>"Wellington", "icp"=>"123456789")
IcpRecord.create("id"=>2, "address1"=>"14 Beach st", "suburb"=>"Smogeville", "city"=>"Auckland", "icp"=>"951648237")
IcpRecord.create("id"=>3, "address1"=>"8 Woodville road", "suburb"=>"Leeston", "city"=>"Canterbury", "icp"=>"984EF5761")
IcpRecord.create("id"=>4, "address1"=>"86 Customhouse Quay", "city"=>"Wellington", "icp"=>"144476500")

RegisterSerial.create("icp_record_id"=>1, "meter_number"=>"654", "register_number"=>"01", "register_decimals"=>0)
RegisterSerial.create("icp_record_id"=>1, "meter_number"=>"654", "register_number"=>"02", "register_decimals"=>0)

RegisterSerial.create("icp_record_id"=>2, "meter_number"=>"384", "register_number"=>"01", "register_decimals"=>3)

RegisterSerial.create("icp_record_id"=>3, "meter_number"=>"953", "register_number"=>"01", "register_decimals"=>2)
RegisterSerial.create("icp_record_id"=>3, "meter_number"=>"953", "register_number"=>"02", "register_decimals"=>2)
RegisterSerial.create("icp_record_id"=>3, "meter_number"=>"953", "register_number"=>"03", "register_decimals"=>3)

RegisterSerial.create("icp_record_id"=>4, "meter_number"=>"860", "register_number"=>"01", "register_decimals"=>2)
RegisterSerial.create("icp_record_id"=>4, "meter_number"=>"861", "register_number"=>"01", "register_decimals"=>2)

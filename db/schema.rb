# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_05_11_235051) do

  create_table "icp_records", force: :cascade do |t|
    t.text "address1"
    t.text "address2"
    t.string "suburb"
    t.string "city"
    t.string "icp", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["icp"], name: "index_icp_records_on_icp", unique: true
  end

  create_table "register_serials", force: :cascade do |t|
    t.integer "icp_record_id"
    t.string "meter_number"
    t.string "register_number"
    t.integer "register_decimals"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "requests", force: :cascade do |t|
    t.string "customer_name"
    t.text "customer_address1"
    t.text "customer_address2"
    t.string "customer_suburb"
    t.string "customer_postcode"
    t.string "customer_city"
    t.string "customer_icp"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "email"
    t.string "phone"
    t.boolean "admin_confirm"
    t.boolean "extra_scrutiny"
  end

  create_table "users", force: :cascade do |t|
    t.string "username"
    t.string "name"
    t.boolean "admin"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  add_foreign_key "register_serials", "icp_records"
end

class AddEmailPhoneToRequests < ActiveRecord::Migration[6.0]
  def change
    add_column :requests, :email, :string
    add_column :requests, :phone, :string
  end
end

class AddAdminConfirmToRequests < ActiveRecord::Migration[6.0]
  def change
    add_column :requests, :admin_confirm, :boolean, :default => false
  end
end

class CreateIcpRecords < ActiveRecord::Migration[6.0]
  def change
    create_table :icp_records do |t|
      t.text :address1
      t.text :address2
      t.string :suburb
      t.string :city
      t.string :icp, :null => false

      t.index :icp, :unique => true

      t.timestamps
    end
  end
end

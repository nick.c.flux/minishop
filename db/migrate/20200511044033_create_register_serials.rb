class CreateRegisterSerials < ActiveRecord::Migration[6.0]
  def change
    create_table :register_serials do |t|
      t.integer :icp_record_id
      t.string :meter_number
      t.string :register_number
      t.integer :register_decimals

      t.timestamps
    end

    add_foreign_key :register_serials, :icp_records
  end
end

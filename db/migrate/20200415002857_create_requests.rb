class CreateRequests < ActiveRecord::Migration[6.0]
  def change
    create_table :requests do |t|
      t.string :customer_name
      t.text :customer_address1
      t.text :customer_address2
      t.string :customer_suburb
      t.string :customer_postcode
      t.string :customer_city
      t.string :customer_icp

      t.timestamps
    end
  end
end

class AddUniqueToUsers < ActiveRecord::Migration[6.0]
  def change
    #change_column :users, :username, :string, :unique => true, :null => false
    add_index :users, :username, unique: true
  end
end

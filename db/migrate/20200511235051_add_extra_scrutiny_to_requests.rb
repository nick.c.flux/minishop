class AddExtraScrutinyToRequests < ActiveRecord::Migration[6.0]
  def change
    add_column :requests, :extra_scrutiny, :boolean, :default => false
  end
end

class Request < ApplicationRecord

  scope :admin_confirmed, -> { where(admin_confirm: true)}
  scope :admin_unconfirmed, -> { where(admin_confirm: false)}

  validates :customer_name,
            presence: { message: "Please provide your name"}
  validates :email,
            presence: { message: "Please provide your email address"},
            format: { with: /.*@.*/, message: "Email must contain an @ symbol"}
  validates :phone,
            presence: { message: "Please provide your phone number"},
            format: { with: /\A[0-9]+\z/, message: "Digits only for phone number"}
  validates :customer_address1,
            presence: { message: "Please provide your street address"}
  validates :customer_postcode,
            presence: { message: "Please provide your postcode"},
            length: { is: 4, message: "Postcodes are 4 digits" }
  validates :customer_city,
            presence: { message: "Please list your city"}
  validates :customer_icp,
            presence: { message: "Please provide your ICP"},
            length: { is: 15, message: "ICP numbers are 15 digits/characters"}
end


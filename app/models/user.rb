class User < ApplicationRecord
  validates :username,
            presence: { message: "Please enter username"},
            uniqueness: {case_sensitive: false, message: " already exists"}

end

class IcpCheckService
  attr_accessor :icp

  def initialize(icp)
    @icp = icp
  end

  def call
    first_char_is_letter? || last_char_is_zero? || has_three_repeated_char?
  end

  private
  #return true if first character is letter
  def first_char_is_letter?
    icp[0,1].count("a-zA-Z") > 0
  end

  #return true if last character is zero
  def last_char_is_zero?
    icp[-1,1] == '0'
  end

  #return true if the icp has a character repeated 3 times in a row
  def has_three_repeated_char?
    icp[/(.)\1\1/] != nil
  end
end
class SessionsController < ApplicationController

  def new
  end

  def create
    @user = User.find_by(username: params[:session][:username])
    if @user
      session[:user_id] = @user.id
      redirect_to @user
    else
      @errors = ActiveModel::Errors.new(self)
      @errors.add(:base, "Username does not exist")
      render 'new'
    end
  end

  def delete
    reset_session
    redirect_to login_url
  end
end

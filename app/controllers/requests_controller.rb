class RequestsController < ApplicationController
  before_action :redirect_unauthorized, except: [:new, :create]

  def index
    @requests = Request.all
  end

  def show
    @request = Request.find(params[:id])
    @icp_records = IcpRecord.all
  end

  def new
    @request = Request.new
  end

  def create
    @request = Request.new(request_params)
    #check if ICP needs admin scrutiny
    if IcpCheckService.new(@request.customer_icp).call
      @request.extra_scrutiny = true
    end

    if @request.save
      redirect_to @request
    else
      render 'new'
    end
  end

  def confirm
    Request.update(params[:request_id], admin_confirm: params[:admin_confirm])
    redirect_to :requests
  end

  private
  def request_params
    params.require(:request).permit(
      :customer_name,
      :email,
      :phone,
      :customer_address1,
      :customer_address2,
      :customer_suburb,
      :customer_postcode,
      :customer_city,
      :customer_icp
    )
  end
end

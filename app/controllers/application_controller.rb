class ApplicationController < ActionController::Base

  helper_method :current_user
  helper_method :user_is_admin?
  helper_method :redirect_unauthorized

  def current_user
    User.find_by(id: session[:user_id])
  end

  def user_is_admin?
    current_user.admin?
  end

  def redirect_unauthorized
    unless current_user && user_is_admin?
      redirect_to :login, notice: "Only administrators can access this page"
    end
  end

end

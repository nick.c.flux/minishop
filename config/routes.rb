Rails.application.routes.draw do
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#delete'
  get 'home/index'

  resources :requests do
    post 'confirm'
  end
  resources :users
  resources :sessions, only: [:create]

  root 'home#index'
end
